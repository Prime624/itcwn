﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class General : MonoBehaviour {

    public Sprite[] carSprites = new Sprite[5];
    public GameObject carPrefab;
    public GameObject dirtSpot;

    // Start is called before the first frame update
    void Start() {
        CarScripts.createCar(carPrefab, carSprites, dirtSpot, 0);
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKey ("escape")) {
             Application.Quit();
        }
    }
}
