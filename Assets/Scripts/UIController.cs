﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
    public Button splashButton;
    // Start is called before the first frame update
    void Start() {
        splashButton.onClick.AddListener(goToSplash);
    }
    void goToSplash() {
        Cursor.visible = true;
        SceneManager.LoadScene("SplashScreen");
    }
}
