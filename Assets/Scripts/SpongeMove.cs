﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpongeMove : MonoBehaviour {
    public AudioSource scrubSound;
    // Start is called before the first frame update
    void Start() {
        // Hides cursor
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update() {
        // Converts the mouse coordinates to game coordinates and moves the sponge to it
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        transform.position = mousePos;
    }

    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "dirt") {
            Destroy(col.gameObject);
            scrubSound.Play();
        }
    }
}
