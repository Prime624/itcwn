﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour {
    public Button playButton, creditsButton;
    // Start is called before the first frame update
    void Start() {
        playButton.onClick.AddListener(startGame);
        creditsButton.onClick.AddListener(goToCredits);
    }

    void startGame() {
        SceneManager.LoadScene("MainScene");
    }

    void goToCredits() {
        SceneManager.LoadScene("CreditsScene");
    }
}
