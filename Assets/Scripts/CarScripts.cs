﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarScripts : MonoBehaviour {
    ScoreKeeper sk;
    Sprite[] carSprites = new Sprite[5];
    GameObject carPrefab;
    GameObject dirtSpot;
    private float speed = .2f;
    private Vector3 destination;
    private Vector3 exitPoint = new Vector3(-25.0f, 0.0f, 0.0f);

    // Start is called before the first frame update
    void Start() {
        sk = GameObject.Find("Score Text").GetComponent<ScoreKeeper>();
        enterScene();
    }

    // Update is called once per frame
    void Update() {
        if (transform.childCount == 0 && destination != exitPoint) {
            exitScene();
            sk.score++;
            sk.updateScore();
        }

        transform.localPosition = Vector3.MoveTowards(transform.localPosition, destination, speed);
        
        if (transform.localPosition == exitPoint) {
            createCar(carPrefab, carSprites, dirtSpot, sk.score * 1);
            Destroy(gameObject);
        }
    }

    void enterScene() {
        destination = new Vector3(0.0f, 0.0f, 0.0f);
    }
    void exitScene() {
        destination = exitPoint;
    }

    public static void createCar(GameObject carPrefab, Sprite[] carSprites, GameObject dirtSpot, int numOfDirtSpots) {
        GameObject parent = Instantiate(carPrefab, new Vector3(25,0,0), Quaternion.identity);
        parent.GetComponent<SpriteRenderer>().sprite = carSprites[Random.Range(0, carSprites.Length)];
        parent.GetComponent<CarScripts>().assignSprites(carPrefab, carSprites, dirtSpot);
        for (int i = 0; i < numOfDirtSpots + 3; i++) {
            Instantiate(dirtSpot, new Vector3(Random.Range(21f, 29f), Random.Range(-2f, 2f), 0), Quaternion.identity, parent.transform);
        }
    }

    void assignSprites(GameObject carPrefab, Sprite[] carSprites, GameObject dirtSpot) {
        this.carPrefab = carPrefab;
        this.carSprites = carSprites;
        this.dirtSpot = dirtSpot;
    }
}
