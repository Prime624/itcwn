﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CreditsScreen : MonoBehaviour {
    public Button backButton;
    // Start is called before the first frame update
    void Start() {
        backButton.onClick.AddListener(backToSplash);
    }

    void backToSplash() {
        SceneManager.LoadScene("SplashScreen");
    }
}
