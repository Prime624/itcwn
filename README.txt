Instructions:
You work at a car wash famed for their speed and efficiency. Wash cars as fast as you can by dragging your sponge over the dirty spots. Time yourself to see how well you do! Cars will get progressively dirtier as word of your quality work spreads.

Asset Sources:
Background image - free for commercial, no attribution required - https://pixabay.com/images/search/road/
Sponge - personal use only (school project) - modified from https://www.kisspng.com/png-sponge-cleaning-car-wash-clip-art-cleaning-graphic-649150/
Scrubbing sound - Attribution Noncommercial License - cropped from https://freesound.org/people/Sinjohnt/sounds/325601/
